package com.company;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class BingoBaby implements Runnable {
    private int number;
    private int rand;
    private String guess;

    public BingoBaby(int number, String guess){

        this.number = number;
        this.guess = guess;

    }

    public void run(){
        System.out.printf("""
                Lets see how long it takes our threads to guess the number. 
                Parameters: Pre selected number = %s
                """, number);

        for (int count = 1; count < 100; count++){
            Random random = new Random();
            this.rand = random.nextInt(100);
            if (count != rand ){
                System.out.print(guess + " tried " + rand + " and is still wrong.  ");
                try{
                    Thread.sleep(number);
                }catch (InterruptedException e){
                    System.err.println(e.toString());
                }

            }
        }
        System.out.println("\n\nFinally guessed the number " + guess + "!\n\n");
    }
}
