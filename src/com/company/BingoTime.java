package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BingoTime {

    public static void main(String[] args) {

        ExecutorService bingoTime = Executors.newFixedThreadPool(3);

        BingoBaby bb1 = new BingoBaby(88, "guess #1");
        BingoBaby bb2 = new BingoBaby(84, "guess #2");
        BingoBaby bb3 = new BingoBaby(22, "guess #3");
        BingoBaby bb4 = new BingoBaby(48, "guess #4");
        BingoBaby bb5 = new BingoBaby(19, "guess #5");

        bingoTime.execute(bb1);
        bingoTime.execute(bb2);
        bingoTime.execute(bb3);
        bingoTime.execute(bb4);
        bingoTime.execute(bb5);

        bingoTime.shutdown();

    }
}
